﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.contador1.procesoTerminado += new EventHandler(MyEventHandlerFunction_StatusUpdated);
            CheckForIllegalCrossThreadCalls = false;
        }
        public void MyEventHandlerFunction_StatusUpdated(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            contador1.moverContador();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            contador1.stop();
        }

        private void txtVelocidad_TextChanged(object sender, EventArgs e)
        {
            contador1.velocidad = Convert.ToInt32(txtVelocidad.Text == "" ? 0 : Convert.ToInt32(txtVelocidad.Text));
        }
    }
}
