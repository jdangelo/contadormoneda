﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Contador
{
  
    public partial class contador : UserControl
    {
        public event EventHandler procesoTerminado;
        public int velocidad = 10;
        public contador()
        {
            InitializeComponent();
        }

        public void moverContador()
        {
            
            backgroundWorker1.RunWorkerAsync();
        }
        public void movePosicion()
        {
            for (int i = 0; i < 60; i++)
            {
                lblPrimero.Location = new Point(lblPrimero.Location.X, lblPrimero.Location.Y - 1);
                Thread.Sleep(velocidad); //PROPIEDAD MANEJADO EN EL TEXTBOX DEL FORMULARIO QUE DETERMINA QUE CUAN RAPIDO SE MUEVE EL LABEL
            }
        }
        public void stop()
        {
            backgroundWorker1.CancelAsync();
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
            for (int i = 100; i < 200; i++)
            {
                //SI SE CANCELO EL HILO PARA EL PROCESO 
                if (this.backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                //SETEO LOS VALORES EN LOS LABEL HACIENDO UN SPLIT PARA PONERLA EN LA POSICION QUE CORRESPONDA
                lblTercero.Text = i.ToString().Substring(0, 1);
                lblSegundo.Text = i.ToString().Substring(1, 1);
                lblPrimero.Text = i.ToString().Substring(2, 1);
                //llamo al metodo que hace el efecto moviendo el eje y del label
                  movePosicion();
                //Thread.Sleep(100);
                // SETEO LA POSICION ORIGINAL DE LOS LABELS
                lblPrimero.Location = new Point(lblPrimero.Location.X, 15);
                lblSegundo.Location = new Point(lblSegundo.Location.X, 15);
                lblTercero.Location = new Point(lblTercero.Location.X, 15);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.Write("Termino");
            this.procesoTerminado(this, new EventArgs());
        }
    }
}
